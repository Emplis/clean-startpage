function isStorageAvailable() {
    try {
        let localStorage = window.localStorage;
        let sessionStorage = window.sessionStorage;

        localStorage.setItem("__availability_test__", "__availability_test__");
        localStorage.removeItem("__availability_test__");
        sessionStorage.setItem("__availability_test__", "__availability_test__");
        sessionStorage.removeItem("__availability_test__");

        return true;
    } catch (e) {
        console.error(`'localStorage' or 'sessionStorage' not available. (${e.message})`);
        return (
            e instanceof DOMException &&
            e.name === "QuotaExceedError" &&
            localStorage && localStorage.length !== 0 &&
            sessionStorage && sessionStorage.length !== 0
        );
    }
}

function getPageEditState() {
    const pageEditState = window.sessionStorage.getItem("pageEditState");
    return pageEditState == "true";
}

function setPageEditState(newState) {
    const settings = window.sessionStorage;
    settings.setItem("pageEditState", newState ? "true" : "false");
}

function getCardEditState() {
    const cardEditState = window.sessionStorage.getItem("cardEditState");
    return cardEditState == "true";
}

function setCardEditState(newState) {
    const settings = window.sessionStorage;
    settings.setItem("cardEditState", newState ? "true" : "false");
}

function getEditedCardId() {
    const editedCardId = window.sessionStorage.getItem("editedCardId");
    return parseInt(editedCardId);
}

function setEditedCardId(newValue) {
    const settings = window.sessionStorage;
    
    if (isNaN(newValue)) {
        return;
    }

    settings.setItem("editedCardId", newValue);
}

function unsetEditedCardId() {
    setEditedCardId(-1);
}

function writeChangeToStorage() {
    let cardsDataStr = JSON.stringify(globalThis._cardsData);
    localStorage.setItem("cards_data", cardsDataStr);
}

function restoreStorage(data) {
    globalThis._cardsData = data;
    writeChangeToStorage();
}

function fetchAllCards() {
    let cardsData = null;
    let cardsStr = localStorage.getItem("cards_data");

    if (cardsStr === null) {
        let emptyCardsData = {"cards": []};
        cardsData = emptyCardsData;
        writeChangeToStorage(); // Init storage
    } else {
        cards = JSON.parse(cardsStr);
        cardsData = cards;
    }

    return cardsData;
}

function getNbrOfCards() {
    return globalThis._cardsData.cards.length;
}

function updateCard(cardId, cardName, cardLink, cardImg) {
    const nbrOfCards = getNbrOfCards();

    if (cardId < 0 || cardId > nbrOfCards) {
        console.error(`Invalid card ID. (${cardId})`);
        return;
    }

    card = {
        "name": cardName,
        "link": cardLink,
        "img": cardImg,
    };

    if (cardId == nbrOfCards) {
        globalThis._cardsData.cards.push(card);
    } else {
        globalThis._cardsData.cards[cardId] = card;
    }

    writeChangeToStorage();
}

function deleteCard(cardId) {
    if (cardId < 0 || cardId >= getNbrOfCards()) {
        console.error(`Invalid card ID. (${cardId})`);
        return;
    }

    globalThis._cardsData.cards.splice(cardId, 1);

    writeChangeToStorage();
}

function getCard(cardId) {
    if (cardId < 0 || cardId > getNbrOfCards()) {
        console.error("Invalid card ID.");
        return;
    }

    return globalThis._cardsData.cards[cardId];
}

function initSessionSettings() {
    let settings = window.sessionStorage;

    settings.setItem("pageEditState", "false");
    settings.setItem("cardEditState", "false");
    settings.setItem("editedCardId", "-1");
}

function initStorage() {
    initSessionSettings();

    window.addEventListener("storage", (e) => {
        const watchedKeys = ["cards_data"];
        let key = e.key;

        if (watchedKeys.indexOf(key) != -1) {
            console.log(`${key} has been updated!`);
            
            if (key === "cards_data") {
                globalThis._cardsData = fetchAllCards();
                reloadAllCards();
            }
        }
    });
}
