const root = document.querySelector(":root")
const themeSwitcher = document.getElementById("theme-switcher")
let userTheme = "dark"

function applyTheme() {
    themeSwitcher.innerText = (userTheme == "light") ? "🌚️" : "🌞️";
    themeSwitcher.setAttribute("title", `Use ${userTheme} theme`);

    if (userTheme === "light") {
        root.style.setProperty("--background-color", "var(--white)");
        root.style.setProperty("--background-color-hsl", "var(--white-hsl)");
        root.style.setProperty("--foreground-color", "var(--black)");
        root.style.setProperty("--foreground-color-hsl", "var(--black-hsl)");
    } else if (userTheme === "dark") {
        root.style.setProperty("--background-color", "var(--black)");
        root.style.setProperty("--background-color-hsl", "var(--black-hsl)");
        root.style.setProperty("--foreground-color", "var(--white)");
        root.style.setProperty("--foreground-color-hsl", "var(--white-hsl)");
    }
}

function switchTheme(is_user_click=false) {
    userTheme = (userTheme == "light") ? "dark" : "light";
    applyTheme();

    if (localStorageAvailable() && is_user_click) {
        localStorage.setItem("theme", userTheme);
    }

    themeSwitcher.blur();
}

themeSwitcher.addEventListener("click", () => {
    switchTheme(true);
});
themeSwitcher.addEventListener("keydown", (e) => {
    // Space or Enter
    if (e.key == " " || e.key == "Enter") {
        switchTheme(true);
    }
});

if (window.matchMedia) {
    // Update emoji on system theme update
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener("change", (e) => {
        let newUserTheme = (!e.matches) ? "light" : "dark";
        if (newUserTheme != userTheme) {
            switchTheme();
        }
    })

    // Change theme variable if the theme is light in CSS
    if (window.matchMedia('(prefers-color-scheme: light)').matches) {
        userTheme = "light";
        applyTheme();
    } else if (isStorageAvailable()) {
        const trueStr = ["true", "True", "TRUE", "1"];

        let storedUserTheme = localStorage.getItem("theme");

        if (storedUserTheme !== null && storedUserTheme !== "") {
            if (storedUserTheme != userTheme) {
                switchTheme();
            }
        }

        let useTrueBlack = localStorage.getItem("useTrueBlack");

        if (useTrueBlack !== null && useTrueBlack !== "") {
            if (trueStr.indexOf(useTrueBlack) != -1) {
                root.style.setProperty("--black", "var(--true-black)");
                root.style.setProperty("--black-hsl", "var(--true-black-hsl)");
            }
        }
    }
}
