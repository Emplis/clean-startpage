function initKeyboardShortcuts() {
    window.addEventListener("keydown", (e) => {
        homePageKeyboardShortcuts(e);

        exportKeyboardShortcuts(e);
        importKeyboardShortcuts(e);
    });

    window.addEventListener("keyup", (e) => {
        editKeyboardShortcuts(e);
    });
}