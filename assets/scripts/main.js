function generateCard(name, link_url, img_url, clickCallback=null, openInNewTab=false, displayName=true, noBackground=false, useDashedBorder=false) {
    let cardsContainer = document.getElementsByClassName("cards-container")[0];

    const trueLink = clickCallback === null;

    let newCard = document.createElement("div");
    let newLogo = document.createElement(trueLink ? "a" : "span");
    let newLineBreak = document.createElement("br");
    let newName = document.createElement("span");
    let newClickableLabel = document.createElement(trueLink ? "a" : "span");

    newCard.classList.add("flex-container", "card");
    newLogo.classList.add("logo");
    newClickableLabel.classList.add("clickable-label");

    if (name != null || img_url != null) {
        name = (name.length > 20) ? name.slice(0, 18) + "..." : name;
        newClickableLabel.innerText = (name != null) ? name : "";
        
        if (img_url !== null && img_url !== "") {
            newLogo.style.setProperty("background-image", "url(" + img_url + ")");
        } else {
            newLogo.style.setProperty("--single-letter-content", "'" + name.charAt(0).toUpperCase() + "'");
        }

        if (trueLink) {
            newClickableLabel.setAttribute("href", link_url);
            newLogo.setAttribute("href", link_url);

            if (openInNewTab) {
                newClickableLabel.setAttribute("target", "_blank");
            }
        } else {
            newClickableLabel.setAttribute("tabindex", "0");
            newLogo.setAttribute("tabindex", "0");
            newClickableLabel.addEventListener("click", () => {
                clickCallback();
                newClickableLabel.blur();
            });
            newLogo.addEventListener("click", () => {
                clickCallback();
                newLogo.blur();
            });
        }

        newCard.appendChild(newLogo);
        newCard.appendChild(newLineBreak);
        newName.appendChild(newClickableLabel);

        if (displayName) {
            newCard.appendChild(newName);
        }

        cardsContainer.appendChild(newCard);
    }
}

function generateAllCards(edit_mode=false) {
    const nbrOfCards = getNbrOfCards();
    let card;
    let link;

    let callback = null;

    for (let i = 0; i < nbrOfCards; i++) {
        card = getCard(i);

        link = card.link;

        if (!edit_mode) {
            try {
                new URL(card.link);
            } catch (e) {
                if (e instanceof TypeError) {
                    link = `https://${link}`;
                } else {
                    console.error(`Unknown error, invalid link. (${link})`);
                }
            }
        } else {
            callback = () => {
                console.log(`Edit card with ID: ${i}`);
                editCardView(i);
            };
        }

        generateCard(card.name, link, card.img, callback);
    }
}

function editCardView(cardId) {
    let editOverlay = document.getElementsByClassName("edit-overlay")[0];

    if (getCardEditState()) {
        return;
    }

    editOverlay.classList.remove("edit-overlay-hide");

    setCardEditState(true);
    setEditedCardId(cardId);

    initCardEditView(cardId);
}

function reloadEditPageView() {
    const editBtn = document.getElementsByClassName("option-edit")[0];
    const cardsContainer = document.getElementsByClassName("cards-container")[0];

    let newCardCallback = () => {
        const nbrOfCards = getNbrOfCards();
        editCardView(nbrOfCards);
    };

    cardsContainer.replaceChildren();

    generateAllCards(true);
    generateCard("+", "", null, newCardCallback, false, false, true, true);

    editBtn.innerText = "❌";
    editBtn.setAttribute("title", "Leave page editing");
}

function editPageView() {
    if (getPageEditState()) {
        return;
    }

    reloadEditPageView();
    
    setPageEditState(true);
}

function exitPageEditView() {
    const editBtn = document.getElementsByClassName("option-edit")[0];
    const cardsContainer = document.getElementsByClassName("cards-container")[0];

    if (!getPageEditState()) {
        return;
    }

    cardsContainer.replaceChildren();

    generateAllCards();

    editBtn.innerText = "✏️";
    editBtn.setAttribute("title", "Edit the page");

    setPageEditState(false);
}

function checkHiddenParams() {
    const trueStr = ["true", "True", "TRUE", "1"];
    const falseStr = ["false", "False", "FALSE", "0"];
    const urlParams = new URL(window.location).searchParams;
    let resetAll = urlParams.get("reset");
    let useTrueBlack = urlParams.get("true_black");
    let reload = false;

    if (resetAll !== null && resetAll !== "" && localStorageAvailable()) {
        if (trueStr.indexOf(resetAll) !== -1) {
            localStorage.clear();
            reload = true;
        }
    }

    if (useTrueBlack !== null && useTrueBlack !== "" && localStorageAvailable()) {
        if (trueStr.indexOf(useTrueBlack) != -1) {
            localStorage.setItem("useTrueBlack", "true");
        } else if (falseStr.indexOf(useTrueBlack) != -1) {
            localStorage.removeItem("useTrueBlack");
        }
        reload = true;
    }

    if (reload) {
        window.location = "index.html";
    }
}

function reloadAllCards() {
    let cardsContainer = document.getElementsByClassName("cards-container")[0];

    cardsContainer.replaceChildren();

    if (getPageEditState() || getNbrOfCards() == 0) {
        editPageView();
    } else {
        generateAllCards();
    }

    if (getCardEditState()) {
        window.location = "index.html";
    }
}

function homePageKeyboardShortcuts(e) {
    const pageEditState = getPageEditState();
    const cardEditState = getCardEditState();
    if (!cardEditState) {
        if (!pageEditState) {
            const keyCode = e.key.charCodeAt(0);

            if (e.ctrlKey && e.key == "e") {
                editPageView();
            } else if (keyCode >= 48 && keyCode <= 57) {
                let cardId = (keyCode - 48 == 0) ? 9 : ((keyCode - 48) - 1);

                if (cardId < getNbrOfCards()) {
                    let card = getCard(cardId);
                    let link = card.link;

                    try {
                        new URL(link);
                    } catch (e) {
                        if (e instanceof TypeError) {
                            link = `https://${link}`;
                        } else {
                            console.error(`Unknown error, invalid link. (${link})`);
                        }
                    }

                    window.location = link;
                }
            }
        } else {
            if (e.key == "Escape") {
                exitPageEditView();
            }
        }
    }
}

function initPageEditBtn() {
    const editBtn = document.getElementsByClassName("option-edit")[0];

    editBtn.addEventListener("click", () => {
        const pageEditState = getPageEditState();
        const cardEditState = getCardEditState();

        if (!cardEditState) {
            if (!pageEditState) {
                editPageView();
            } else {
                exitPageEditView();
            }
        }
    });
}

function initPage() {
    if (!isStorageAvailable()) {
        document.getElementsByClassName("no-storage-overlay").classList.remove("display-none");
        return;
    }

    initStorage();

    globalThis._cardsData = fetchAllCards();

    if (getNbrOfCards() == 0) {
        editPageView();
    } else {
        generateAllCards();
    }

    initPageEditBtn();

    checkHiddenParams();

    initKeyboardShortcuts();
}
