function downloadFile(jsonData) {
    const link = document.createElement("a");

    link.href = `data:text/json,${jsonData}`;
    link.download = "cards_export.json";

    link.click();
}

function exportKeyboardShortcuts(e) {
    if (e.ctrlKey && e.key == "s") {
        e.preventDefault();
        downloadFile(JSON.stringify(_storage_data));
    }
}
