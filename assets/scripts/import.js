function importKeyboardShortcuts(e) {
    const importDialog = document.getElementById("import-dialog");

    if (e.ctrlKey && e.key == "o") {
        e.preventDefault();
        importDialog.showModal();
    }
}

function initImport() {
    const importDialog = document.getElementById("import-dialog");
    const importInput = document.getElementById("import-input");
    const importCancel = document.getElementById("import-cancel");

    let fileDataURL = file => new Promise((resolve, reject) => {
        let fr = new FileReader();

        fr.addEventListener("load", () => {
            resolve(fr.result);
        });
        fr.addEventListener("error", () => {
            reject();
        });

        fr.readAsText(file);
    });
    
    importDialog.addEventListener("close", () => {
        console.log(`Dialog closed. (${importDialog.returnValue})`);
    });
    
    importCancel.addEventListener("click", () => {
        importDialog.close();
    });
    
    importInput.addEventListener("change", () => {
        if (importInput.files.length >= 1) {
            fileDataURL(importInput.files[0])
            .then((data) => {
                let parsedData = JSON.parse(data);
                restoreStorage(parsedData);
                window.location = "index.html";
            })
            .catch((err) => {
                console.error(`Failed to import file. (${err})`);
            });
        }
    });
}

initImport();
