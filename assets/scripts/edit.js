function getCardId() {
    let cardId = getEditedCardId();

    if (cardId != -1) {
        let nbrOfCards = getNbrOfCards();

        if (cardId >= 0 && cardId <= nbrOfCards) {
            return cardId;
        } else {
            console.error("Invalid card ID. (" + cardId + ")");
        }
    }

    return null;
}

function exitCardEditView() {
    const editOverlay = document.getElementsByClassName("edit-overlay")[0];

    const name = document.getElementsByClassName("edit-card-name")[0];
    const link = document.getElementsByClassName("edit-card-link")[0];

    const imgInput = document.getElementsByClassName("edit-card-img")[0];
    const imgValue = document.getElementsByClassName("edit-card-img-value")[0];
    const imgPreview = document.getElementsByClassName("edit-img-btn")[0];

    const saveBtn = document.getElementsByClassName("edit-card-save")[0];
    const cancelBtn = document.getElementsByClassName("edit-card-cancel")[0];
    const deleteBtn = document.getElementsByClassName("edit-card-delete")[0];

    if (!getCardEditState()) {
        return;
    }

    name.value = "";
    link.value = "";
    imgValue.value = "";
    imgPreview.style.setProperty("background-image", "none");

    editOverlay.classList.add("edit-overlay-hide");

    imgInput.disabled = true;
    saveBtn.disabled = true;
    cancelBtn.disabled = true;
    deleteBtn.disabled = true;

    setCardEditState(false);
    unsetEditedCardId();
}

function fileReadSuccess(data) {
    let imgValue = document.getElementsByClassName("edit-card-img-value")[0];
    let imgPreview = document.getElementsByClassName("edit-img-btn")[0];
    
    imgValue.value = data;

    imgPreview.style.setProperty("background-image", "url(" + data + ")");
}

function fileReadFailed(error) {
    console.error(error);
}

function editKeyboardShortcuts(e) {
    if (getCardEditState()) {
        const cardId = getCardId();

        const name = document.getElementsByClassName("edit-card-name")[0];
        const link = document.getElementsByClassName("edit-card-link")[0];
        const imgValue = document.getElementsByClassName("edit-card-img-value")[0];

        const saveBtn = document.getElementsByClassName("edit-card-save")[0];

        if (e.key == "Escape") {
            exitCardEditView();
        } else if (e.key == "Enter" && !saveBtn.disabled) {
            console.log("Saving data...");
            updateCard(cardId, name.value, link.value, imgValue.value);

            reloadEditPageView();
            exitCardEditView();
        } else if (e.key == "Delete") {
            deleteCard(cardId);

            reloadEditPageView();
            exitCardEditView();
        }
    }
}

function initCardEditView(cardId) {
    const name = document.getElementsByClassName("edit-card-name")[0];
    const link = document.getElementsByClassName("edit-card-link")[0];
    
    const imgInput = document.getElementsByClassName("edit-card-img")[0];
    const imgValue = document.getElementsByClassName("edit-card-img-value")[0];
    const imgPreview = document.getElementsByClassName("edit-img-btn")[0];
    const imgReplaceBtn = document.getElementsByClassName("edit-img-btn")[0];

    const saveBtn = document.getElementsByClassName("edit-card-save")[0];
    const cancelBtn = document.getElementsByClassName("edit-card-cancel")[0];
    const deleteBtn = document.getElementsByClassName("edit-card-delete")[0];

    const nbrOfCards = getNbrOfCards();

    if (cardId < nbrOfCards) {
        let card = getCard(cardId);

        name.value = card.name;
        link.value = card.link;
        imgValue.value = card.img;
        imgPreview.style.setProperty("background-image", "url(" + imgValue.value + ")");
    }

    saveBtn.disabled = false;
    saveBtn.addEventListener("click", () => {
        console.log("Saving data...");
        updateCard(cardId, name.value, link.value, imgValue.value);

        reloadEditPageView();
        exitCardEditView();
    });

    cancelBtn.disabled = false;
    cancelBtn.addEventListener("click", () => {
        exitCardEditView();
    });

    if (cardId == nbrOfCards) {
        deleteBtn.disabled = true;
    } else {
        deleteBtn.disabled = false;
    }

    deleteBtn.addEventListener("click", () => {
        deleteCard(cardId);

        reloadEditPageView();
        exitCardEditView();
    });

    let fileDataURL = file => new Promise((resolve, reject) => {
        let fr = new FileReader();

        fr.addEventListener("load", () => {
            resolve(fr.result);
        });
        fr.addEventListener("error", () => {
            reject();
        });

        fr.readAsDataURL(file);
    });

    imgInput.disabled = false;
    imgInput.addEventListener("change", () => {
        /* Disable save btn while the img is loading */
        saveBtn.disabled = true;
        if (imgInput.files.length >= 1) {
            fileDataURL(imgInput.files[0])
            .then(data => {
                fileReadSuccess(data);
                saveBtn.disabled = false;
            })
            .catch(err => fileReadFailed(err));
        }
    });

    imgReplaceBtn.addEventListener("click", () => {
        imgInput.click();
    });
}
