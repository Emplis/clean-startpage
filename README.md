# Clean Startpage

A clean startpage to quickly access everyday websites.

All data are stored on your browser using `localStorage` API. Data can be downloaded and imported in JSON.

## Keyboard shortcuts

| Shortcut | Action |
| --- | --- |
| <kbd>Ctrl</kbd>+<kbd>s</kbd> | Download data (JSON file) |
| <kbd>Ctrl</kbd>+<kbd>o</kbd> | Open import window |
| <kbd>Ctrl</kbd>+<kbd>e</kbd> | Edit the page |
| <kbd>1</kbd>, ..., <kbd>0</kbd> | Open first site to the tenth |

## Screenshots

![Screenshot with dark theme](screenshots/screenshot-dark.png){width=60%}*Screenshot with dark theme*

![Screenshot with light theme](screenshots/screenshot-light.png){width=60%}*Screenshot with light theme*

![Screenshot of edit menu with dark theme](screenshots/screenshot-dark-edit.png){width=60%}*Screenshot of the editing menu with dark theme*

## Ressources

- [IBM Plex](https://github.com/IBM/plex): [License](https://github.com/IBM/plex/blob/master/LICENSE.txt)
